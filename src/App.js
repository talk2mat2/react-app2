import React from "react";
import Continent from "./component/Continent";
import "./style.css";
import { ApolloClient, InMemoryCache, gql, useQuery } from "@apollo/client";

const ListContinents = gql`
  {
    continents {
      name
      countries {
        name
        languages {
          name
        }
      }
    }
  }
`;

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: "https://countries.trevorblades.com",
});

const App = () => {
  const { data, loading, error } = useQuery(ListContinents, { client });
  const [result, setResult] = React.useState([]);


//function to generate nested mock data
   //n = depth of JSON tree
  //m = number of children in each node
  const SliceData = (n, m) => {
    const SlicedDataArray = data
      ? data.continents.slice(0, n).map((continents, index) => {
          let continent = {
            ...continents,
            countries: continents["countries"].slice(0, m),
          };
          return continent;
        })
      : [];
    console.log(SlicedDataArray);
    setResult(SlicedDataArray)
    return SlicedDataArray;
  };



  React.useEffect(() => {
    // we take 6 continent and first 5 countries child of a country
    SliceData(6, 5);
  }, [data]);
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-lg-10 col-10 bg-grey content-section">
          {loading ? (
            <h4>loading...</h4>
          ) : error ? (
            <h4>Error occured , make sure you have a working network</h4>
          ) : (
            // getContinets(5, 5)
        result.map((continent,index)=> <Continent key={index} continent={continent}  />)
          )}
        </div>
      </div>
    </div>
  );
};

export default App;
