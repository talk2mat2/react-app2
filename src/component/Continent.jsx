import React from "react";

const Continent = (props) => {
  const [showChilds, setShowChilds] = React.useState(false);
  const {continent:{name,countries}} =props

  const handleShowChildes = () => {
    showChilds && !setShowChilds(false);
    !showChilds && setShowChilds(true);
  };

  const mapCountries= ()=>{
      return countries&&countries.length>0?
      countries.map(country=>
         <li className="child-nodes">{country['name']}</li>):null
  }
  return (
    <div onClick={handleShowChildes} className="continent-items">
      <div className="continent"><p>{name}</p></div>
      {showChilds ? (
        <ul>
          {mapCountries()}
        </ul>
      ) : null}
    </div>
  );
};

export default Continent;
